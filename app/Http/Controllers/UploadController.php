<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class UploadController extends Controller
{
    /**
     * @OA\Post(
     *     path="/upload/file",
     *    tags={"upload"},
     *     summary="Upload files to the system.",
     *     operationId="uploadFile",
     *     description="Return path file on the system.",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(property="file", type="string", format="binary"),
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/User"),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User not found",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User not found."
     *              ),
     *         ),
     *     ),
     * )
     */
    public function index(Request $request)
    {
        try {
            $name = $this->setNameFile($request->file->extension());
            $path = $request->file->storeAs('files', $name, 'public');
            return new JsonResponse(['path' => '/' . $path], 200);
        } catch (\Throwable $th) {
            report($th);
            throw new HttpResponseException(
                response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'file' => $th->getMessage(),
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
    }

    /**
     * @OA\Post(
     *     path="/upload/multiple-file",
     *    tags={"upload"},
     *     summary="Upload files to the system.",
     *     operationId="uploadMultipleFile",
     *     description="Return path file on the system.",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *              type="object",
     *                  @OA\Property(
     *                      property="files[]",
     *                      type="array",
     *                      @OA\Items(type="string", format="binary")
     *                  )
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/User"),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User not found",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User not found."
     *              ),
     *         ),
     *     ),
     * )
     */
    public function multiple(Request $request)
    {
        try {
            $result = [];
            foreach ($request->file('files') as $key => $file) {
                $name = $this->setNameFile($file->extension());
                $path = $file->storeAs('files', $name, 'public');
                array_push($result, '/' . $path);
            }
            return new JsonResponse($result, 200);
        } catch (\Throwable $th) {
            report($th);
            throw new HttpResponseException(
                response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'file' => $th->getMessage(),
                    ]
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
    }

    /**
     * Create new name file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function setNameFile($extension)
    {
        $time = time();
        $str_rand = \Str::random(12);
        return "{$str_rand}_{$time}.{$extension}";
    }
}